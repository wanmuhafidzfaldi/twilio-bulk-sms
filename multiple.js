

var accountSid = 'AC949a0e4e90624b7e13ebd00a7103f82d'; // Your Account SID from www.twilio.com/console
var authToken = '147be9c628e55905bdec73ad132a2896';   // Your Auth Token from www.twilio.com/console
var mSid = 'IS2281fbef2f047a982c4ca7c6a1e1a3e5';

const twilio = require('twilio')(
  accountSid,
  authToken
);

const body = 'Tester';
const numbers = ['+6285210300240', '+6285210300240', '+6285210300240', 
'+6285210300240', '+6285210300240', '+6285210300240', '+6285210300240', 
'+6285210300240', '+6285210300240', '+6285210300240', '+6285210300240', 
'+6285210300240', '+6285210300240', '+6285210300240', '+6285210300240', 
'+6285210300240', '+6285210300240', '+6285210300240', '+6285210300240',
'+6285210300240', '+6285210300240', '+6285210300240', '+6285210300240'];

const service = twilio.notify.services(mSid);

const bindings = numbers.map(number => {
  return JSON.stringify({ binding_type: 'sms', address: number });
});

notification = service.notifications
  .create({
    toBinding: bindings,
    body: body,
  })
  .then((notification) => {
    console.log(notification);
  })
  .catch(err => {
    console.error(err);
  });